# Use an official Python runtime as the base image
FROM python:3.11-alpine

ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=5000
# Set the working directory in the container
WORKDIR /app

# Copy the application code into the container
COPY . .

# Install the application dependencies
RUN python3 -m venv venv
RUN source venv/bin/activate

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install python-dotenv

# Expose the port on which the application will run
EXPOSE 5000

# Start the application
COPY cmds.sh /cmds.sh
RUN chmod +x /cmds.sh
ENTRYPOINT ["/bin/sh", "-c", "/cmds.sh"]


