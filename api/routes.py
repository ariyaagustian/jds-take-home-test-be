from flask import Blueprint
from db import db
from flask_sqlalchemy import SQLAlchemy
from controllers.penderita_diabetes_controller import PenderitaDiabetesController
from middleware.auth import token_required

api_blueprint = Blueprint('api', __name__,url_prefix='/api')

# endpoint get seluruh data penderita diabetes melitus
@api_blueprint.route('/penderita-diabetes')
def get_penderita_diabetes():
    return PenderitaDiabetesController.get_all()

# endpoint get data jumlah penderita diabetes melitus tahunan
@api_blueprint.route('/penderita-diabetes/tahunan')
@token_required
def get_penderita_diabetes_tahunan():
    return PenderitaDiabetesController.get_anual_data()

# endpoint get data jumlah penderita diabetes per kabupaten/kota pada tahun tertentu
@api_blueprint.route('/penderita-diabetes/kabupaten-kota/<int:tahun>')
@token_required
def get_penderita_diabetes_kabupaten_kota(tahun):
    return PenderitaDiabetesController.get_penderita_diabetes_kabupaten_data(tahun)