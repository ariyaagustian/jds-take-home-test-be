import os
from flask import Flask, jsonify
from db import db
from dotenv import load_dotenv
from flask_migrate import Migrate

from seeders.seeds import seed_data
from models.penderita_diabetes import PenderitaDiabetes
from api.routes import api_blueprint
from flask_cors import CORS

load_dotenv()

app = Flask(__name__)
CORS(app)
app.app_context().push()
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
migrate = Migrate(app, db)
migrate.init_app(app, db)

seed_data()

app.register_blueprint(api_blueprint)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
