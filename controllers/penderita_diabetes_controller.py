from flask import jsonify
from sqlalchemy import text
from db import db
from models.penderita_diabetes import PenderitaDiabetes

class PenderitaDiabetesController:
  def get_all():
    penderita_diabetes = PenderitaDiabetes.query.all()
    data = [p.to_dict() for p in penderita_diabetes]
    return jsonify(
        {
            'status': 200,
            'data': data
        })
    
  def get_anual_data():
      query = text('SELECT kode_provinsi, nama_provinsi, tahun, sum(jumlah_penderita_dm) as jumlah_penderita_dm FROM penderita_diabetes group by kode_provinsi, nama_provinsi, tahun order by jumlah_penderita_dm desc')
      result = db.session.execute(query)
      data = result.fetchall()
      list = []
      for row in data:
          list.append({
              'kode_provinsi': row[0],
              'nama_provinsi': row[1],
              'tahun': row[2],
              'jumlah_penderita_dm': row[3],
          })
      
      return jsonify(
        {
            'status': 200,
            'data': list
        })
      
  def get_penderita_diabetes_kabupaten_data(tahun):
      # ambil data jumlah penderita diabetes per kabupaten/kota pada tahun tertentu dari database
      data = PenderitaDiabetes.query.filter_by(tahun=tahun).group_by(
          PenderitaDiabetes.id,
          PenderitaDiabetes.kode_provinsi,
          PenderitaDiabetes.nama_provinsi,
          PenderitaDiabetes.kode_kabupaten_kota,
          PenderitaDiabetes.nama_kabupaten_kota,
          PenderitaDiabetes.jumlah_penderita_dm,
          PenderitaDiabetes.satuan,
          PenderitaDiabetes.tahun
      ).all()

      # buat list yang berisi informasi jumlah penderita diabetes per kabupaten/kota pada tahun tertentu
      result = []
      for item in data:
          result.append({
              'nama_kabupaten_kota': item.nama_kabupaten_kota,
              'jumlah_penderita_dm': item.jumlah_penderita_dm,
          })

      # kembalikan data dalam bentuk json
      return jsonify(
        {
            'data': {
              'tahun' : tahun,
              'list' : result
            },
            'status': 200
        })