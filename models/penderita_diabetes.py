# models/penderita_diabetes.py

from db import db

class PenderitaDiabetes(db.Model):
    __tablename__ = 'penderita_diabetes'
    id = db.Column(db.Integer, primary_key=True)
    kode_provinsi = db.Column(db.String(2), nullable=False)
    nama_provinsi = db.Column(db.String(50), nullable=False)
    kode_kabupaten_kota = db.Column(db.String(4), nullable=False)
    nama_kabupaten_kota = db.Column(db.String(50), nullable=False)
    jumlah_penderita_dm = db.Column(db.Integer, nullable=False)
    satuan = db.Column(db.String(10), nullable=False)
    tahun = db.Column(db.Integer, nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'kode_provinsi': self.kode_provinsi,
            'nama_provinsi': self.nama_provinsi,
            'kode_kabupaten_kota': self.kode_kabupaten_kota,
            'nama_kabupaten_kota': self.nama_kabupaten_kota,
            'jumlah_penderita_dm': self.jumlah_penderita_dm,
            'satuan': self.satuan,
            'tahun': self.tahun
        }
