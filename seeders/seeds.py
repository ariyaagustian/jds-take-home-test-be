import json
from models.penderita_diabetes import PenderitaDiabetes
from db import db
from sqlalchemy import text

def seed_data():
    print('Seeding data...')
    seed_penderita_diabetes()

def seed_penderita_diabetes():
    db.reflect()
    # check if data already exist
    query = text("SELECT EXISTS ( SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename  = 'penderita_diabetes')")
    exist = db.session.execute(query).fetchone()[0]
    if exist:
        print("Exist") 
        if db.session.query(PenderitaDiabetes).count() > 0:
            print('Data Penderita Diabetes already exists, skipped.')
            return
        with open('./seeders/jumlah_penderita_diabetes_melitus_jawa_barat.json') as f:
            data = json.load(f)['data']
            db.session.add_all([
                PenderitaDiabetes(
                    kode_provinsi=penderitaDiabetes['kode_provinsi'],
                    nama_provinsi=penderitaDiabetes['nama_provinsi'],
                    kode_kabupaten_kota=penderitaDiabetes['kode_kabupaten_kota'],
                    nama_kabupaten_kota=penderitaDiabetes['nama_kabupaten_kota'],
                    jumlah_penderita_dm=penderitaDiabetes['jumlah_penderita_dm'],
                    satuan=penderitaDiabetes['satuan'],
                    tahun=penderitaDiabetes['tahun']
                )
                for penderitaDiabetes in data
            ])
            db.session.commit()
            print('Data Penderita Diabetes seeded.')
