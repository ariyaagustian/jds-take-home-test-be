import unittest
import requests
import os
from dotenv import load_dotenv

load_dotenv()

class test_api(unittest.TestCase):
    headers = {
        'Authorization': 'Bearer '+str(os.environ.get('TOKEN'))
    }
    
    def test_get_penderita_diabetes(self):
        response = requests.get(str(os.environ.get('API_URL'))+'penderita-diabetes')
        self.assertEqual(response.status_code, 200)
        print('Response OK (200)')
        print("Test api get seluruh data penderita diabetes melitus success")
        
    def test_get_penderita_diabetes_tahunan(self):
        response = requests.get(str(os.environ.get('API_URL'))+'penderita-diabetes/tahunan', headers=self.headers)
        self.assertEqual(response.status_code, 200)
        print('Response OK (200)')
        print("Test get data jumlah penderita diabetes melitus tahunan success")
        
    def test_get_penderita_diabetes_kabupaten_kota(self):
        response = requests.get(str(os.environ.get('API_URL'))+'penderita-diabetes/kabupaten-kota/2020', headers=self.headers)
        self.assertEqual(response.status_code, 200)
        print('Response OK (200)')
        print("Test api get data jumlah penderita diabetes per kabupaten/kota pada tahun 2020 success")

if __name__ == '__main__':
    tester = test_api()
    tester.test_get_penderita_diabetes()
    tester.test_get_penderita_diabetes_tahunan()
    tester.test_get_penderita_diabetes_kabupaten_kota()
